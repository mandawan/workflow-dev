# The very Uzful workflow to setup

## Préambule
Bonjour monde, dans ce fichier, je vais vous proposer des suggestions d'outils,
de conventions, de logiciels, etc...; pour adopter une méthodologie commune au
sein d'une équipe.
Suivre une méthodologie commune peut éviter les pertes de temps lors de la
collaborations entre plusieurs membres de l'équipe qui ne travaillent pas
forcément sur le même projet au même moment.

Ces propositions sont faites par rapport aux expérience que j'ai eue lors
du développement du Very Uzful Linkomatic en collaboration avec Manu et Ben.


### Legende

:white_check_mark: signifie que je recommande

:no_entry_sign: signifie que c'est intéressant mais je pense qu'il y a mieux

:grey_question: Signifie que j'aimerais qu'il y ait reflexion sur la techno

## Versionning

Lors de la soirée charette, ce qui nous a sauvé les miches a été de s'imposer
un nommage clair des branches et une méthodologie stricte à l'aide d'un outil.
* **Jet Brains' VCS** :white_check_mark:
  * N'importe quel éditeur/IDE JetBrain vient avec un VCS
  (logiciel de gestion de versions) très complet et visuel. En effet, il permet
  de résoudre les conflits liés au merge de plusieurs branches très facilement.
  Grâce à son code couleur compréhensible et les 3 versions de code présentes
  (gauche la version locale, milieu la version finale et droite la
  version distante), il est possible en temps réel de voir le résultat de
  la résolution des conflits et même de modifier le code tout en profitant
  de l'IDE.
  ![vcs](https://d3nmt5vlzunoa1.cloudfront.net/idea/files/2016/03/idea-vcs-merge.png)
  Grace à cette solution, il n'y a pas eu de perte de code et les merges se
  sont réalisés rapidement et sans soucis.
* **Sourcetree** :grey_question:
  * Logiciel de versionning par Atlassian (bitbucket), évite de tout faire via
  le terminal et permet de gagner du temps
  * Utilisé avec gitflow (un exemple [ici](https://youtu.be/nHMiLdKe1BI?t=11m56s))
* **Gitflow** :grey_question:
  * Gitflow est une convention universelle
  * C'est une sorte de framework qui utilise git
    * on utilise les commandes gitflow qui elles même génèrent des commandes git
    pour bien organiser son travail et éviter les conflits
    * Fonctionne avec un system de [conventions](http://nvie.com/files/Git-branching-model.pdf) très bien
    expliquées [ici](http://nvie.com/posts/a-successful-git-branching-model/) et [ici](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)
    * en gros on peut faire [tout ça](https://danielkummer.github.io/git-flow-cheatsheet/img/git-flow-commands.png)
    * Adfab en parle [ici](http://connect.adfab.fr/dev/developper-dans-les-meilleures-conditions-en-2016-retour-dexperience-dun-jeune-developpeur) qui amène à [cet article](https://fr.slideshare.net/cecilia_bossard/git-flow-in-action)
    * Grafikart [aussi](https://www.grafikart.fr/formations/git/git-flow)
* **Uzful very own git convention** :white_check_mark:
  * On peut s'inspirer du modèle de gitflow sans forcément l'installer. Lors du rush
linkomatic nous avions un system similaire. Une branche master qui est la
branche avec un code fonctionnel dessus. Une branche staging (ou release) qui
accueil les différentes branches des développeurs. Elle permet de merger
différentes branches pour ensuite tester le code des devs. Si tout est ok, on
merge sur master (normalement sans conflits) qui est prêt pour le déploy.
Chaque développeur travaille sur sa branche qui provient du master à jour.
Elle est nommée suivant le ticket à réaliser.


## IDEs && Text Editors

Comme dit dans la section précédente, les IDE JetBrains contiennent des outils
intéressants. En plus de leur VCS, ils ont aussi une fonction pour gérer
une base de données SQL (et des plugins existent pour gérer du NoSQL)
On peut aussi les configurer de manière à ce qu'ils corrigent automatiquement
notre syntaxe et ainsi respecter des conventions (je pense
notamment à l'ES6 et l'ESlinter qui devenait insupportable avec Atom).
L'avantage, c'est que ces configs sont exportables et utilisables avec toute
la gamme JetBrains et chaque langage est configurable.
Ps: Les licenses sont gratuites pour les étudiants

* **Intellij IDEA** :white_check_mark:
  * [Intellij IDEA](https://www.jetbrains.com/idea/)
  est l'éditeur spécialisé **JAVA** mais comprend très bien le
  **javascript, html, css, php, sass, pyhton, ruby** et a même des plugins
  **Vue.js.**
  Je l'ai aussi utilisé pour faire du **Meteor**, mais j'ai eu quelques soucis
  d'indexing.
* **Webstorm** :no_entry_sign:
  * [Webstorm](https://www.jetbrains.com/webstorm/)
  comprend les mêmes langages front mais ne connait pas les langages
  backs. Étant donné que nous sommes plutot FullStack chez Uzful, ce n'est
  peut être pas la peine de se priver de ces langages.
* **Atom** :white_check_mark:
  * Léger et open source, Atom s'ouvre en 3 secondes et connait bien le JS.
  Utile pour modifier des fichiers rapidement ou faire du web simple. Très
  utile pour écrire de la doc en markdown (comme ce document) grâce à sa
  rapide fonction `preview` en effectuant `ctrl shift m`.
  Il fonctionne tout de même très bien avec **Vue.js** même si une grosse
  config lui serait préférable pour respecter l'ESlinter.

## Déploiement Continue

Il peut être intéressant de mettre en place un outil de déploiement continue,
afin d'avoir le même environnement de travail sur nos machines, sur le serveur
de testes et sur le serveur de production.
Cela permettra de lancer des tests unitaires qui confirmeront que notre code
fonctionne et lancera automatiquement (ou non) une mise en production.

<!-- TODO Faire un benchmark plus poussé de ces deux outils -->
* **SASS**
  * **Circle CI** :white_check_mark:
    * À décrire ...
  * **Travis** :grey_question:
    * À décrire ...
  * **CodeShip** :grey_question:
    * À décrire ...
* **Auto Hébergement**
  * **Jenkins** :no_entry_sign:
    * À décrire ...


## Code convention

Dans les différents projets que nous avons réalisé, j'ai remarqué que mes
collègues avaient parfois du mal à reprendre mon code et vice versa.
Nous perdions beaucoup de temps à faire de la relecture et de comprendre la
logique de ce qui a été réalisé.

* **Versionning**
  * Cf paragraphe versionning

* **CSS**
Lors de différentes sessions de code sur le linkomatic, j'ai remarqué qu'il y
a eu plusieurs incompréhensions au niveau du css et même de la répétition de
code :scream:
  * **Javascript**
    * **Typescript** :grey_question:
      * La team dev Uzful a un passé sur des langages plus ou moins typés
      (C, Python, etc..). Nous collaborons en alternance sur nos projets,
      rajouter de la rigueur de code (en plus d'un linter es6) ne peut être
      que bénéfique et peut aider à comprendre plus rapidement du code
      qui n'a pas été fait par sois même.
      Ps: Vue.js a un
      [composant typescript](https://github.com/vuejs/vue-class-component)
  * **SASS** :white_check_mark:
    * Je propose de continuer à utiliser **sass** qui a permis un code propre
    ainsi qu'une réutilisation de fichiers pour éviter de tout réécrire.
    Cependant le problème se trouvait plutôt dans les conventions de nommage
    et dans les façons d'inclure certains fichiers.
    * `stylesheets/main.scss` contient tous les fichiers à loader qui se
    trouvent dans `stylesheets/common/` et qui seront appelés
    à chaque page
    * Chaque fichier html doit loader `mains.scss` et un `index.scss` qui lui
    correpond. Cela évite les problèmes de scope et de nom de classe similaires
    dans différents fichiers.
    Pour être sur de ne pas avoir de scope, il faut wrapper son code html
    dans un container au nom **unique** Ex:
    ```html
    <div class="nom-unique-container">
        <div class="title">
          <h1>Hello World</h1>
        </div>
    </div>
    ```
    ```html
    <div class="second-nom-unique-container">
        <div class="title">
          <h1>Bonjour monde</h1>
        </div>
    </div>
    ```
    ```
    .nom-unique-container {
      .title {
        color: orangered;
      }
    }

    .second-nom-unique-container {
      .title {
        color: red;
      }
    }
    ```
    Il y a beau avoir 2 `div` ayant la class `title`, les couleurs ne seront pas
    les mêmes suivant les containers.
    Chaque `index.scss` doit donc commencer par
    ```css
    .mon-container {
      /* Code goes here */
    }
    ```

* **FrontEnd**
  * **Vue.js** :white_check_mark:
    * Après nos deux derniers projets, il est clair que Vue.js a montré qu'il
    était puissant, moderne, rapide et scallable. Il sait se coupler
    parfaitement aux APIs et réagir de manière stable. L'équipe dev
    connait son fonctionnement et a adopté une architecture se basant sur
    ce [boiler plate](https://github.com/vuejs-templates/webpack)
    * **Webpack** :white_check_mark:
      * Pratique pour pimper son site et le loading de fichiers (qu'ils soient
      en sass, csv, etc...)
    * **Babel** :white_check_mark:
      * En attendant que l'ES6 soit supporté par tous les navigateurs, Babel
      permet de coder du JS proprement ! :ok_hand:
    * **Vuex** :grey_question:
      * Je l'ai trouvé très puissant mais peut être faut-il se pencher sur la
      [doc de la dernière version](https://vuex.vuejs.org/en/getting-started.html)
      pour en comprendre la puissance et l'étendu.
      Peut être qu'un local storage est suffisant.
    * **Vue Router** :white_check_mark:
      * [Très simple d'utilisation](https://router.vuejs.org/en/).
      Permet de créer un système de routing dans
      l'application qui s'y intègre très facilement et rapidement.
      Configurable facilement.
    * **Debug** :white_check_mark:
      * Une belle alternative aux `console.log()`, permet d'afficher des
      messages dans un environnement spécial qui sera caché aux utilisateurs.
      Fini les `removed console.log()` dans les commits.
    * **gsap** :white_check_mark:
      * Bibliothèque pour faire des anims propres rapidement.
* **BackEnd**
  * **Node.js** :grey_question:
  La techno a permis un code capable de gérer plusieurs taches en même
  temps grâce à l'asynchrone de javascript. Ceci a permis de faire d'autres
  taches en même temps pour avoir une exécution rapide. J'aimerais tout même
  expérimenter d'autres technologies pour la création d'API REST.
    * **Express** :white_check_mark:
      * Depuis la mise en production de l'API App Weekend, il n'y a eu aucun
      crash. Le system de routing est facile d'utilisation ainsi que la
      gestion d'erreurs.
    * **ES5** :no_entry_sign:
      * Syntaxe vieillissante et trop permissive. M'a fait perdre du temps alors
      que certaines taches sont plus rapides et élégantes avec la nouvelle
      norme JS. Elle est de plus supportée nativement par les dernières versions
      de **node.js**, il n'y a même plus besoin de passer par **Babel**.
    * **bottleneck** :white_check_mark:
      * Limiter pour javascript. Très simple d'utilisation, a permis de réguler
      mes appels aux APIs comme les appels externes reçus sur mon API.
    * **node-schedule** :white_check_mark:
      * Se basant sur le principe des taches `chron` du shell, node-schedule
      permet de déclencher des évènements à des moments précis.
  * **Graphql**
    * [Techno](https://developer.github.com/early-access/graphql/)
    à suivre de près pour construire des API et faire un minimum de requêtes
    pour récupérer un maximum d'infos.
      * Développé par Facebook


* **Front & Back**
  * **Laravel** :grey_question:
    * MVC est une architecture que nous connaissons et comprenenons bien.
    Le MVC est fiable et beaucoup utilisé. Pas besoin de passer du temps à
    réapprendre l’archi
    * Laravel [se plug avec vue.js en front](https://github.com/laravue/laravue)
    ([tuto sur medium à regarder](https://medium.com/laravel-news/advanced-front-end-setup-with-vue-js-laravel-e9fbd7e89fe2#.acvld0tce))
    * PHP 7 est devenu fiable et puissant. Bonne raison de se remettre à cette
    techno qui déborde de resources sur internet.
  * **Apollo**
    * Une bonne partie du coeur de la dev team de Meteor
    [l'avait quittée](https://voice.kadira.io/its-time-thank-you-bye-meteor-115dd649a8#.msyaa28k4)  et qu'ils étaient sur un [nouveau projet](https://blog.meteor.com/the-meteor-chef-an-introduction-to-apollo-b1904955289e#.qgv3fcjuu)
    qui s'appelle Apollo
    * Regarder du côté de [ce repo](https://github.com/Akryum/vue-apollo)
    pour un plug avec vue.js
