# Retour sur le développement du **very uzful linkomatic**

## Préambule

<!-- *faire résumé du  projet* -->

[The Very Uzful Linkomatic](linkomatic.uzful.fr)
est un projet qui est sorti fin janvier 2017. Il avait
pour but de servir de carte de voeux de l'agence.
Son pricine ? Proposer à l'aide d'une navigation intuitive une sélection des
meilleurs liens pour réaliser sa veille pro principalement dans le milieu des
agences de communication.

<!-- *mettre en lumière les problématiques du sujet et ses enjeux* -->

La technologie à utiliser devait nous permette de récupérer des
informations données par des personnes non tech, de les trier, de les mettre en
page et de pouvoir continuer à alimenter cette base de données.
Traduit en problématique tech, cela donne les besoins suivants:
<!-- *énoncer et expliquer rapidement choix des technos* -->

Il nous fallait une technologie qui nous permette d'ajouter de la logique
poussée en front, de faire du traitement de données, de réaliser une single
page application (contrainte présente dans le cahier des charges) et qui
s'appuie idéalement sur javascript (langage connu par tous les devs de l'agence).
Nous nous sommes donc tournés vers **Vue.js** qui répondait à tous ces
critères.


## Technologies
* **FrontEnd**
<!-- TODO changer text de présentation de vue.js  -->
  * **Vue.js** :white_check_mark:
    * Après nos deux derniers projets, il est clair que Vue.js a montré qu'il
    était puissant, moderne, rapide et scallable. Il sait se coupler
    parfaitement aux APIs et réagir de manière stable. L'équipe dev
    connait son fonctionnement et a adopté une architecture se basant sur
    ce [boiler plate](https://github.com/vuejs-templates/webpack)
    * **Architecture**
      * On a utilisé une convention. Tous les fichiers front allaient dans
      `/src`. On y retrouve le `main.js` qui prépare **Vue.js** (config de ses
      extensions comme vue router, vuex, etc..); `App.vue` qui contient l'app et
      4 dossiers:
        * **assets**
          * contient nos csv à parser
        * **components**
          * contient toutes pages à afficher avec la logique Vue.js ainsi que
          leur css. Chaque composant a son propre dossier avec tous les fichiers
          nécessaires dedans.
        * **lib**
          * contient les fonctions réutilisées dans plusieurs parties du code
        * **stylesheets**
          * contient le css réutilisé dans plusieurs parties du code
    * **Modules + Packages**
      * **Webpack** :white_check_mark:
        * Pratique pour pimper son site et le loading de fichiers (qu'ils soient
        en sass, csv, etc...)
      * **Babel** :white_check_mark:
        * En attendant que l'ES6 soit supporté par tous les navigateurs, Babel
        permet de coder du JS proprement ! :ok_hand:
      * **Vuex** :grey_question:
        * Je l'ai trouvé très puissant mais peut être faut-il se pencher sur la
        [doc de la dernière version](https://vuex.vuejs.org/en/getting-started.html)
        pour en comprendre la puissance et l'étendu.
        Peut être qu'un local storage est suffisant.
      * **Vue Router** :white_check_mark:
        * [Très simple d'utilisation](https://router.vuejs.org/en/).
        Permet de créer un système de routing dans
        l'application qui s'y intègre très facilement et rapidement.
        Configurable facilement.
      * **Debug** :white_check_mark:
        * Une belle alternative aux `console.log()`, permet d'afficher des
        messages dans un environnement spécial qui sera caché aux utilisateurs.
        Fini les `removed console.log()` dans les commits.
      * **gsap** :white_check_mark:
        * Bibliothèque pour faire des anims propres rapidement.

* **BackEnd**
  * **PHP 5** a été la solution la plus simple pour intégrer de la logique back
  dans l'app. Le code se déploie facilement sur les serveurs et permet de
  cacher des données sensibles tels que des tokens. Pour ce projet, nous avons
  utilisé PHP pour se servir de l'API google drive afin de créer des entrées
  sur un google sheet qui contient des suggestions d'utilisateurs.

* **Déploiement**
  * **npm run build + FTP**

## Impressions générales
***


Le post processing appliqué à l'arrière plan du linkomatic à été réalisé en threejs, en se basant sur le [BadTvShader](https://github.com/felixturner/bad-tv-shader) de Felix Turner, dont voici un capture d'écran :

![screenshot.jpg](https://raw.githubusercontent.com/felixturner/bad-tv-shader/master/example/res/screenshot.jpg)

Pour l'intégrer au projet nous avons dû le convertir en Ecmascript 6. Pour cela nous avons utilisé le module [three-effectcomposer-es6](https://github.com/superhighfives/three-effectcomposer-es6) pour accéder aux RenderPass, ShaderPass et CopyShader qui compose le BadTvEffect.

##To do
* Optimiser le webgl sur mobile
*
